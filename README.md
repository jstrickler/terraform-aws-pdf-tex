# AWS Serverless TeX Pipeline for PDF Processing

This Terraform module creates an end-to-end serverless pipeline for processing TeX packages into PDF files.  

The full deployment package includes the following:
- Input S3 Bucket - Other services can upload TeX ZIP packages to this bucket.
- Input SQS - Lambda processor listens to this
- Input SQS DLQ - Recovery queue for processing failures.
- PDF Processing Lambda - A full TeX Live distro as a Lambda function (https://gitlab.com/jstrickler/texlive-full-lambda)
- Output S3 - Processing lambda uploads the generated PDF
- Output SNS - Publishes completion events. Subscribe to this topic with your own event bus or queues.

## External Setup

This module uses a [Lambda Container](https://aws.amazon.com/blogs/aws/new-for-aws-lambda-container-image-support/) as the PDF processor/generator.  I've provided a [TexLive Lambda image on DockerHub](https://gitlab.com/jstrickler/texlive-full-lambda).  To host this on AWS ECR, do the following steps:

- Create a new ECR repository on your AWS account to host the image.
- `docker pull strickjb/texlive-full-lambda:latest`
- Follow the "push commands" provided within ECR (example below):
  - `docker tag strickjb/texlive-full-lambda:latest xxxxxxxxxxxx.dkr.ecr.us-east-1.amazonaws.com/your-repostitory:latest`
  - `docker push xxxxxxxxxxxx.dkr.ecr.us-east-1.amazonaws.com/your-repostitory:latest`

If you follow the above steps then you should now have a hosted image for processing PDFs.  Copy the Image URI and provide it to as a variable to this module.  That's all the external setup that is required.  

Technically, this pipelines is completely agnostic to the technology within Lambda that is processing the PDF.  You can use anything that you want as long as it implements the [Lambda CI runtime](https://docs.aws.amazon.com/lambda/latest/dg/images-create.html).

## Workflow

Overview: 

- Any ZIP file uploaded to the S3 input bucket will automatically processed to a PDF and the resulting file will be uploaded to the S3 output bucket
- Metadata from the ZIP can be copied to the resulting output PDF.  Useful for matching output files back to their intended purpose for processing.

```mermaid
graph TB
  subgraph "PDF TeX Pipeline"

    UPLOAD["Uploader (ex. API)"] -- Upload file to S3 --> S3-IN
    S3-IN["Input S3 Bucket"] -- S3 sends a message to SQS --> SQS-IN
    SQS-IN["Input SQS Queue"] -- Lambda picks up message --> LAMBDA["Lambda: TeX Live Full Processor"]
    LAMBDA -- Writes PDF to S3 output bucket --> S3-OUT["Output S3 Bucket"]
    S3-OUT -- Publishes completion events that a PDF was generated --> SNS-OUT["Output SNS Topic"]
  end 

  style SQS-IN fill:#e7c9a9,stroke:#555
  style SNS-OUT fill:#e7c9a9,stroke:#555
  style S3-IN fill:#aaf0d1,stroke:#555
  style S3-OUT fill:#aaf0d1,stroke:#555
```

<!-- BEGIN_TF_DOCS -->
## Requirements

No requirements.

## Providers

| Name                                              | Version |
| ------------------------------------------------- | ------- |
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a     |

## Modules

No modules.

## Resources

| Name                                                                                                                                                           | Type     |
| -------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------- |
| [aws_iam_policy.s3_write_access_output](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy)                                | resource |
| [aws_iam_role.processor_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role)                                            | resource |
| [aws_iam_role_policy_attachment.processor_s3_write](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment)    | resource |
| [aws_lambda_event_source_mapping.processor_sqs_input](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_event_source_mapping) | resource |
| [aws_lambda_function.processor](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function)                                   | resource |
| [aws_s3_bucket.input](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket)                                                   | resource |
| [aws_s3_bucket.output](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket)                                                  | resource |
| [aws_s3_bucket_notification.input](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_notification)                         | resource |
| [aws_s3_bucket_notification.output_notification](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_notification)           | resource |
| [aws_sns_topic.output](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sns_topic)                                                  | resource |
| [aws_sns_topic_policy.output](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sns_topic_policy)                                    | resource |
| [aws_sqs_queue.input](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sqs_queue)                                                   | resource |
| [aws_sqs_queue.input_dlq](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sqs_queue)                                               | resource |
| [aws_sqs_queue_policy.input](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sqs_queue_policy)                                     | resource |

## Inputs

| Name                                                                                         | Description                                                                                                                                                        | Type     | Default | Required |
| -------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------ | -------- | ------- | :------: |
| <a name="input_image_uri"></a> [image\_uri](#input\_image\_uri)                              | Image URI to TeX Processor on AWS ECR.  (ex. 'xxxxxxxxxxxx.dkr.ecr.us-east-1.amazonaws.com/texlive-full-lambda:latest').                                           | `string` | n/a     |   yes    |
| <a name="input_input_bucket_name"></a> [input\_bucket\_name](#input\_input\_bucket\_name)    | S3 bucket where the TeX files are dropped in a ZIP format.  Anything that is uploaded to this bucket will be processed to a PDF and saved in the S3 output bucket. | `string` | n/a     |   yes    |
| <a name="input_output_bucket_name"></a> [output\_bucket\_name](#input\_output\_bucket\_name) | S3 bucket where the generated PDFs are copied after successful conversions.                                                                                        | `string` | n/a     |   yes    |

## Outputs

| Name                                                                       | Description |
| -------------------------------------------------------------------------- | ----------- |
| <a name="output_output_topic"></a> [output\_topic](#output\_output\_topic) | n/a         |
<!-- END_TF_DOCS -->