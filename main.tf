locals {
  lambda_timeout = 60
}

resource "aws_lambda_function" "processor" {
  function_name = "pdf-tex-processor"
  description   = "Generates and writes PDFs to a S3 bucket by receiving TeX packages from a S3 input bucket."
  package_type  = "Image"
  image_uri     = var.image_uri
  memory_size   = 1536 // sweet spot for cost savings + performance.
  publish       = true
  role          = aws_iam_role.processor_role.arn
  timeout       = local.lambda_timeout
  environment {
    variables = {
      OutputBucket = var.output_bucket_name
    }
  }
}

resource "aws_lambda_event_source_mapping" "processor_sqs_input" {
  event_source_arn = aws_sqs_queue.input.arn
  function_name    = aws_lambda_function.processor.arn
  batch_size       = 1 // always use 1 lambda invocation to 1 document.
}

resource "aws_iam_role" "processor_role" {
  name = "pdf-tex-processor-role"
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/service-role/AWSLambdaSQSQueueExecutionRole",
    "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly",
    "arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess"
  ]
  assume_role_policy = <<-EOF
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Effect": "Allow",
          "Action": "sts:AssumeRole",
          "Principal": {
            "Service": "lambda.amazonaws.com"
          }
        }
      ]
    }
    EOF
}

resource "aws_iam_policy" "s3_write_access_output" {
  name   = "pdf-tex-write-to-s3-policy"
  policy = <<-EOF
    {
        "Version": "2012-10-17",
        "Statement": [
            {
              "Effect": "Allow",
              "Action": "s3:PutObject",
              "Resource": "${aws_s3_bucket.output.arn}/*"
            }
        ]
    }
    EOF
}

resource "aws_iam_role_policy_attachment" "processor_s3_write" {
  role       = aws_iam_role.processor_role.name
  policy_arn = aws_iam_policy.s3_write_access_output.arn
}
