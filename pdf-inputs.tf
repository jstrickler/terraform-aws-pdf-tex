resource "aws_s3_bucket" "input" {
  bucket = var.input_bucket_name

  lifecycle_rule {
    id      = "auto-purge"
    enabled = true

    transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }

    transition {
      days          = 60
      storage_class = "GLACIER"
    }

    expiration {
      days = 90
    }
  }
}

resource "aws_s3_bucket_notification" "input" {
  bucket = aws_s3_bucket.input.id
  queue {
    queue_arn = aws_sqs_queue.input.arn
    events = [
      "s3:ObjectCreated:*"
    ]
  }
}

resource "aws_sqs_queue" "input" {
  name                       = "pdf-tex-input"
  visibility_timeout_seconds = local.lambda_timeout
  redrive_policy = jsonencode({
    "deadLetterTargetArn" : aws_sqs_queue.input_dlq.arn
    "maxReceiveCount" : 1
  })
}

resource "aws_sqs_queue" "input_dlq" {
  name                       = "pdf-tex-input-dlq"
  visibility_timeout_seconds = local.lambda_timeout
  message_retention_seconds  = 1209600
}

# Allows S3 Notifications 
resource "aws_sqs_queue_policy" "input" {
  queue_url = aws_sqs_queue.input.id
  policy    = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": "*",
      "Action": "sqs:SendMessage",
      "Resource": "${aws_sqs_queue.input.arn}",
      "Condition": {
        "ArnEquals": { "aws:SourceArn": "${aws_s3_bucket.input.arn}" }
      }
    }
  ]
}
POLICY
}
