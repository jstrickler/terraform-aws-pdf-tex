variable "input_bucket_name" {
  description = "S3 bucket where the TeX files are dropped in a ZIP format.  Anything that is uploaded to this bucket will be processed to a PDF and saved in the S3 output bucket."
  type        = string
}

variable "output_bucket_name" {
  description = "S3 bucket where the generated PDFs are copied after successful conversions."
  type        = string
}

// AWS requires container images to live on ECR for Lambda deployments.
// A base image lives on Docker Hub - https://hub.docker.com/repository/docker/strickjb/texlive-full-lambda
// It's the responsibility of the calling module to create an ECR repository and push the appropriate image to ECR.
variable "image_uri" {
  description = "Image URI to TeX Processor on AWS ECR.  (ex. 'xxxxxxxxxxxx.dkr.ecr.us-east-1.amazonaws.com/texlive-full-lambda:latest')."
  type        = string
}
